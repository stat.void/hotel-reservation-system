﻿namespace HotelAppServerSide.Data
{
    public static class HelperFunctions
    {
        /// <summary>
        /// Get a EET time zone converted DateTime. Mainly relevant for cancelling reservations, as in all other cases,
        /// if the user is not in this time zone, then using their local timezone would be more convenient to reduce confusion.
        /// However, since cancelling reservations needs to be more than 3 days of time left, the user shouldn't be able to convert their 
        /// time zone to temporarily bypass limits up to ~12 hours.
        /// </summary>
        public static DateTime GetEETDateTimeToday()
        {
            var dt = DateTime.UtcNow;

            TimeZoneInfo tz;
            try
            {
                tz = TimeZoneInfo.FindSystemTimeZoneById("E. Europe Standard Time");
            }
            catch (Exception)
            {
                // This exception catch is technically bad, but time limitations and this would be massive overkill.
                tz = TimeZoneInfo.Local;
            }
            
            var utcOffset = new DateTimeOffset(dt, TimeSpan.Zero);
            DateTime trueNow = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset)).LocalDateTime.Date;
            return trueNow;
        }

        public static float GetReservationCost(float dayCost, DateTime? start, DateTime? end)
        {
            if (start == null || 
                end == null)
                return -1;

            int dayCount = end.Value.Subtract(start.Value).Days;
            return MathF.Round(dayCost * dayCount, 2);
        }

        
        /// <summary>
        /// Code borrowed from the internet to add to add to a list element in a sorted order. 
        /// For reservations stored in HotelRooms to order them by StartDate.
        /// </summary>
        /// <typeparam name="T"> Any type, basically whatever the list contains of. </typeparam>
        /// <param name="this"> The list to be added to. </param>
        /// <param name="item"> The item to be added. </param>
        public static void AddSorted<T>(this List<T> @this, T item) where T : IComparable<T>
        {
            if (@this.Count == 0)
            {
                @this.Add(item);
                return;
            }
            if (@this[^1].CompareTo(item) <= 0)
            {
                @this.Add(item);
                return;
            }
            if (@this[0].CompareTo(item) >= 0)
            {
                @this.Insert(0, item);
                return;
            }
            int index = @this.BinarySearch(item);
            if (index < 0)
                index = ~index;
            @this.Insert(index, item);
        }
        
    }
}

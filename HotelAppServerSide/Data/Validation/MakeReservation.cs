﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace HotelData.DatabaseAccess
{
    public class MakeReservation
    {
        [Required]
        public string? FirstName { get; set; }

        [Required]
        public string? LastName { get; set; }

        [Required]
        [MinLength(11, ErrorMessage = "The given ID is too short")]
        [MaxLength(11, ErrorMessage = "The given ID is too long")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "A Person ID only consists of numbers")]
        public string? PersonID { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string? Email { get; set; }

        [Required]
        public string? RoomName { get; set; }

        [Required]
        public DateTime? StartDate { get; set; } = DateTime.Today;

		[Required]
        public DateTime? EndDate { get; set; } = DateTime.Today.AddDays(1);
	}
}

﻿using HotelAppServerSide.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HotelData.DatabaseAccess
{
    public class HotelDataContext(DbContextOptions<HotelDataContext> options, IConfiguration configuration) : IdentityDbContext<ApplicationUser>(options)
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<HotelRoom> HotelRooms { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        protected readonly IConfiguration Configuration = configuration;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(Configuration.GetConnectionString("DefaultConnection"));
        }

    }
}

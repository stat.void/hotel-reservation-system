﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelData.DatabaseAccess
{
    public class HotelRoom
    {
        [Key]
        public required string RoomNumber { get; set; }
        public int BedCount { get; set; }
        public float DayCost { get; set; }

        [NotMapped]
        public bool ShowDetails { get; set; } = false;

        [NotMapped]
        public Reservation? OngoingReservation = null;

        [NotMapped]
        public readonly List<Reservation> UpcomingReservations = [];

        [NotMapped]
        public readonly List<Reservation> PastReservations = [];


        /// <summary>
        /// Verify if the provided date range overlaps with any date ranges in this room.
        /// There are 6 potential cases when comparing date ranges, but the only 2 cases that are desired and return a false
        /// are either if the provided end date is smaller than the compared start date, or vice versa with start larger than end
        /// </summary>
        public bool DoesDateRangeOverlap(DateTime startDate, DateTime endDate)
        {
            if (OngoingReservation != null)
            {
                if (!(endDate < OngoingReservation.StartDate || startDate > OngoingReservation.EndDate))
                    return true;
            }

            foreach (Reservation res in UpcomingReservations)
            {
                if (!(endDate < res.StartDate || startDate > res.EndDate))
                    return true;
            }

            return false;
        }

    }
}

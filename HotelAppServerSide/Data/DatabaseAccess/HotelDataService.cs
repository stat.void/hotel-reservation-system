﻿using HotelAppServerSide.Components.Pages;
using HotelAppServerSide.Data;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace HotelData.DatabaseAccess
{
    /// <summary>
    /// <para> Service that allows accessing and updating all local database data. </para>
    /// <para> The intended use case is to inject this service into every component that needs to be able to read / update this data. </para>
    /// <para> Components using this should also inject IDbContextFactory<HotelDataContext> that can be passed on here. </para>
    /// </summary>
    public class HotelDataService
    {
        /// <summary>
        /// <para> Publicly readable Dictionary of clients that have used hotel services. </para>
        /// <para> Key is their person ID, Value is their saved data. </para>
        /// </summary>
        public Dictionary<string, Client> Clients { get; private set; } = [];

        /// <summary>
        /// <para> List of all reservations that have been made throughout the hotels existence. </para>
        /// <para> Contains past, present and future reservations. </para>
        /// </summary>
        public List<Reservation> ClientReservations { get; private set; } = [];

        /// <summary>
        /// <para> List of all rooms that the hotel has. </para>
        /// <para> Each room also has internal knowledge on any currently ongoing and future reservations. </para>
        /// </summary>
        public List<HotelRoom> HotelRooms { get; private set; } = [];

        /// <summary>
        /// Refresh all database data for the person viewing through the browser.
        /// </summary>
        /// <param name="context">The database from where local data is fetched</param>
        public async Task UpdateReadData(HotelDataContext context)
        {
            if (context == null)
                return;

            HotelRooms = await context.HotelRooms.ToListAsync();
            Clients = await context.Clients.ToDictionaryAsync(t => t.PersonID, t => t);
            ClientReservations = await context.Reservations.ToListAsync();

            DateTime today = DateTime.Now;

            // Go through all reservations and give hotel rooms knowledge on their reservation status.
            foreach (Reservation res in ClientReservations)
            {
                // Case 1 -> Ongoing reservation
                if (today >= res.StartDate && today <= res.EndDate)
                    HotelRooms.First(room => room.RoomNumber == res.RoomName)
                        .OngoingReservation = res;

                // Case 2 -> Upcoming reservation
                else if (today < res.StartDate)
                    HelperFunctions.AddSorted(
                        HotelRooms.First(room => room.RoomNumber == res.RoomName).UpcomingReservations,
                        res);

                // Case 3 -> All others are past reservations
                else
                    HelperFunctions.AddSorted(
                        HotelRooms.First(room => room.RoomNumber == res.RoomName).PastReservations,
                        res);
            }

            foreach (HotelRoom room in HotelRooms)
            {
                room.UpcomingReservations.OrderBy(t => t.StartDate).ToList();
            }
        }

        /// <summary>
        /// Perform an asyncrhonous query to add a new reservation and its client.
        /// </summary>
        /// <param name="factory"> Reference to database internal data </param>
        /// <param name="newRes"> The parameters of the reservation from which to add a new client (if needed) and reservation. </param>
        /// <returns> A bool to determine if this task succeeded. False if the last database refresh detects an overlapping reservation. </returns>
        public async Task<(bool, FailType)> AddNewReservation(IDbContextFactory<HotelDataContext> factory, MakeReservation? newRes)
        {
            if (newRes == null)
                return (false, FailType.NoReservation);

            // Check to verify that the user didn't manage to leave any data unfilled.
            if (string.IsNullOrWhiteSpace(newRes.FirstName) ||
                string.IsNullOrWhiteSpace(newRes.LastName) ||
                 string.IsNullOrWhiteSpace(newRes.PersonID) ||
                string.IsNullOrWhiteSpace(newRes.Email) ||
                string.IsNullOrWhiteSpace(newRes.RoomName) ||
                newRes.StartDate == null ||
                newRes.EndDate == null)
                return (false, FailType.MissingData);

            newRes.FirstName = newRes.FirstName.Trim();
            newRes.LastName = newRes.LastName.Trim();
            newRes.Email = newRes.Email.Trim().ToLower();
            newRes.PersonID = newRes.PersonID.Trim();

            // Check that the Person ID is valid
            if (newRes.PersonID.Length != 11 ||
                !long.TryParse(newRes.PersonID.Trim(), out long val))
                return (false, FailType.PersonIDInvalid);

            string dateString =
                $"{1800 + 100 * MathF.Floor((int.Parse(newRes.PersonID[..1]) - 1) / 2f) +
                int.Parse(newRes.PersonID.Substring(1, 2))}-" +
                $"{newRes.PersonID.Substring(3, 2)}-" +
                $"{newRes.PersonID.Substring(5, 2)}";
            
            // Some of the options that can be checked for person id is that the date is valid, and that it doesn't make a date that hasn't happened yet.
            if (!DateTime.TryParseExact(dateString, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateValue) ||
                dateValue > DateTime.Today)
                return (false, FailType.PersonIDInvalid);

            // Check that email is valid
            if (!new EmailAddressAttribute().IsValid(newRes.Email))
                return (false, FailType.EmailInvalid);

            var context = await factory.CreateDbContextAsync();
            await UpdateReadData(context);
            
            if (Clients.TryGetValue(newRes.PersonID, out Client? value))
            {
                // Check to verify that client data matches if it exists
                if (value.FirstName != newRes.FirstName ||
                value.LastName != newRes.LastName ||
                value.Email != newRes.Email)
                {
                    await context.DisposeAsync();
                    return (false, FailType.ClientDataNotMatch);
                }


                // And alternatively, that the client doesn't already have an ongoing/upcoming reservation
                foreach (HotelRoom room in HotelRooms)
                {
                    if (room.OngoingReservation?.PersonID == value.PersonID ||
                        room.UpcomingReservations.Any(x => x.PersonID == value.PersonID))
                    {
                        await context.DisposeAsync();
                        return (false, FailType.UserAlreadyHasReservation);
                    }  
                }
            } 

            // Last database check to ensure no overlapping times
            if (HotelRooms.First(t => t.RoomNumber == newRes.RoomName)
                .DoesDateRangeOverlap((DateTime)newRes.StartDate, (DateTime)newRes.EndDate))
            {
                await context.DisposeAsync();
                return (false, FailType.TimeOverlap);
            }
                

            Client client = new(newRes.FirstName, newRes.LastName, newRes.PersonID, newRes.Email);
            Reservation reservation = new(null, newRes.RoomName, newRes.PersonID, (DateTime)newRes.StartDate, (DateTime)newRes.EndDate);

            if (!Clients.ContainsKey(client.PersonID))
                context.Clients.Add(client);

            context.Reservations.Add(reservation);

            await context.SaveChangesAsync();
            await context.DisposeAsync();

            return (true, FailType.None);
        }

        /// <summary>
        /// Perform an asynchronous query to delete a reservation.
        /// </summary>
        /// <param name="factory">Context to the database this app is connected to</param>
        /// <param name="res">The reservation to delete.</param>
        /// <returns>Whether or not the deletion was successful, and what the reason is in case of failure.</returns>
        public async Task<(bool, FailType)> DeleteReservation(IDbContextFactory<HotelDataContext> factory, Reservation res)
        {
            if (res == null)
                return (false, FailType.MissingData);

            bool allowed = true;
            DateTime today = HelperFunctions.GetEETDateTimeToday();

            // The delete button is already hidden, but additional checks can help prevent ways to bypass it.
            // Check start date rules
            if (((DateTime)res.StartDate).Subtract(today).Days <= 3)
                allowed = false;

            // Check end date rules
            if (today.Date > res.EndDate.Date)
                allowed = true;

            if (!allowed)
                return (false, FailType.NotAllowed);

            var context = await factory.CreateDbContextAsync();
            context.Reservations.Remove(res);
            await context.SaveChangesAsync();

            await UpdateReadData(context);
            await context.DisposeAsync();

            return (true, FailType.None);
        }
    }

    public enum FailType
    {
        None,
        NoReservation,
        PersonIDInvalid,
        EmailInvalid,
        ClientDataNotMatch,
        MissingData,
        TimeOverlap,
        UserAlreadyHasReservation,
        NotAllowed
    }
}

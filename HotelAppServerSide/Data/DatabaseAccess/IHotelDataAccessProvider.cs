﻿namespace HotelData.DatabaseAccess
{
    public interface IHotelDataAccessProvider
    {
        public void AddReservation(Reservation reservation);
        public void DeleteReservation(int personID);
        public Reservation GetReservation(int personID);
        public List<Reservation> GetAllReservations();

        public void AddClient(Client client);
        public void DeleteClient(int personID);
        public Client GetClient(int personID);
        public List<Client> GetAllClients();

        public HotelRoom GetRoom(string roomNumber);
        public List<HotelRoom> GetAllRooms();

        //Hey, when displaying data, consider a manual day calculation on how many days will a person be there.
        // Maybe also make 2 tables, currently ongoing occupations (with remaining days, 0 being smallest)
        // and upcoming/past ones.

    }
}

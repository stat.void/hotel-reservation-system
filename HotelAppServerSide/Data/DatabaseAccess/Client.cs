﻿using System.ComponentModel.DataAnnotations;

namespace HotelData.DatabaseAccess
{
    public class Client(string firstName, string lastName, string personID, string email)
	{
		public string FirstName { get; set; } = firstName;
		public string LastName { get; set; } = lastName;
		[Key]
		public string PersonID { get; set; } = personID;
		public string Email { get; set; } = email;
	}
}

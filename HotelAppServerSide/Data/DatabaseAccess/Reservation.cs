﻿using System.ComponentModel.DataAnnotations;

namespace HotelData.DatabaseAccess
{
    public class Reservation(int? index, string roomName, string personID, DateTime startDate, DateTime endDate) : IComparable<Reservation>
	{
		[Key]
		public int? Index { get; set; } = index;

		[Required]
		public string RoomName { get; set; } = roomName;
		[Required]
		public string PersonID { get; set; } = personID;
		[Required]
        public DateTime StartDate { get; set; } = startDate;
		[Required]
        public DateTime EndDate { get; set; } = endDate;

		public string GetStartDateString()
			=> GetDate(StartDate);

		public string GetEndDateString()
			=> GetDate(EndDate);

		private string GetDate(DateTime? date)
		{
			if (date == null)
				return "No Date";

			return date.Value.ToShortDateString();
		}

        public int CompareTo(Reservation? other)
        {
            if (other == null)
                return 1;

            return StartDate.CompareTo(other.StartDate);
        }
    }
}

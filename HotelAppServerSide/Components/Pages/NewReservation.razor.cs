﻿using HotelAppServerSide.Components.Account.Pages.Manage;
using HotelData.DatabaseAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MudBlazor;
using System.ComponentModel.DataAnnotations;

namespace HotelAppServerSide.Components.Pages
{
    public partial class NewReservation
    {
        protected bool ShowCreate = false;
        protected bool ShowNoRoomsFound = false;
        protected bool ShowClientDataNotMatch = false;
        protected bool ShowAlreadyHasReservation = false;
        protected bool ShowReservationLastSecondOverlap = false;
        protected bool ShowReservationSuccess = false;

        protected bool[] ShowErrors = [false, false, false, false];

        protected bool CheckButtonDisabled = false;
        protected bool CreateButtonDisabled = false;

        protected MakeReservation MakeReservation { get; set; } = new();
        protected readonly List<HotelRoom> AvailableRooms = [];

        private readonly MudTheme Theme = new();

        protected override Task OnInitializedAsync()
        {
            MakeReservation = new();
            return Task.CompletedTask;
        }

        protected Task OnStartDateChanged(DateTime? value)
        {
            if (value == null)
                return Task.CompletedTask;

            ShowCreate = false;
            ShowNoRoomsFound = false;
            ShowClientDataNotMatch = false;
            ShowAlreadyHasReservation = false;
            ShowReservationLastSecondOverlap = false;
            AvailableRooms.Clear();


            MakeReservation.RoomName = "";
            MakeReservation.StartDate = value;
            

            if (MakeReservation.EndDate != null &&
                MakeReservation.EndDate <= MakeReservation.StartDate)
				MakeReservation.EndDate = MakeReservation.StartDate?.AddDays(1);

            return Task.CompletedTask;
        }

        protected Task OnEndDateChanged(DateTime? value)
        {
            if (value == null)
                return Task.CompletedTask;

            ShowCreate = false;
            ShowNoRoomsFound = false;
            ShowClientDataNotMatch = false;
            ShowAlreadyHasReservation = false;
            ShowReservationLastSecondOverlap = false;
            AvailableRooms.Clear();

            

            MakeReservation.RoomName = "";
            MakeReservation.EndDate = value;

            return Task.CompletedTask;
        }

        protected async Task CheckAvailability()
        {
            ShowReservationSuccess = false;

            AvailableRooms.Clear(); // This is more of a just in case here.

            if (MakeReservation.StartDate == null || MakeReservation.EndDate == null)
                return;
            
            CheckButtonDisabled = true;

            var context = await HotelDataContextFactory.CreateDbContextAsync();
            await DataService.UpdateReadData(context);
            await context.DisposeAsync();

            foreach (HotelRoom room in DataService.HotelRooms)
                if (!room.DoesDateRangeOverlap((DateTime)MakeReservation.StartDate, (DateTime)MakeReservation.EndDate))
                    AvailableRooms.Add(room);

            if (AvailableRooms.Count == 0)
                ShowNoRoomsFound = true;

            CheckButtonDisabled = false;
        }

        protected void OnAvailableRowClicked(TableRowClickEventArgs<HotelRoom> tableRowClickEventArgs)
        {
            MakeReservation.RoomName = tableRowClickEventArgs.Item.RoomNumber;
            ShowCreate = true;
        }

        /// <summary>
        /// <para> Create and submit a new client and their reservation to the database. </para>
        /// <para> Should be called when it's been verified that the client does not have any ongoing or upcoming reservations. </para>
        /// </summary>
        protected async Task CreateNewClientReservation()
        {
            
            ShowClientDataNotMatch = false;
            ShowAlreadyHasReservation = false;
            ShowReservationLastSecondOverlap = false;

            ShowErrors[0] = string.IsNullOrWhiteSpace(MakeReservation.FirstName);
            ShowErrors[1] = string.IsNullOrWhiteSpace(MakeReservation.LastName);
            ShowErrors[2] = string.IsNullOrWhiteSpace(MakeReservation.PersonID);
            ShowErrors[3] = string.IsNullOrWhiteSpace(MakeReservation.Email);

            if (ShowErrors.Any(x => x))
                return;

            CreateButtonDisabled = true;
            (bool successful, FailType reason) = await DataService.AddNewReservation(HotelDataContextFactory, MakeReservation);

            // If fail type is Missing Data, then it should be a case of forced data manipulation that shouldn't be possible
            if (successful)
            {
                ShowReservationSuccess = true;
                MakeReservation = new();
                ShowCreate = false;
            }

            else 
                switch (reason)
                {
                    case FailType.ClientDataNotMatch:
                        ShowClientDataNotMatch = true;
                        break;
                    case FailType.TimeOverlap:
                        ShowReservationLastSecondOverlap = true;
                        break;
                    case FailType.UserAlreadyHasReservation:
                        ShowAlreadyHasReservation = true;
                        break;
                    case FailType.PersonIDInvalid:
                        ShowErrors[2] = true;
                        break;
                    case FailType.EmailInvalid:
                        ShowErrors[3] = true;
                        break;
                    default:
                        break;
                }

            CreateButtonDisabled = false;
        }
    }
}

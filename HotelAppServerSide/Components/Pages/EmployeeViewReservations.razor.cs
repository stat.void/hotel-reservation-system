﻿using HotelAppServerSide.Data;
using HotelData.DatabaseAccess;
using static MudBlazor.CategoryTypes;

namespace HotelAppServerSide.Components.Pages
{
    public partial class EmployeeViewReservations
    {

        protected bool ShowCurrentReservations = false;
        protected bool ShowAllReservations = false;
        private string searchString1 = "";

        protected override async Task OnInitializedAsync()
        {
            var context = await  HotelDataContextFactory.CreateDbContextAsync();
            await DataService.UpdateReadData(context);
            await context.DisposeAsync();
        }

        // You might be able to combine all three choices into a single table to view data. Just keep in mind that maybe you want
        // A choice to be openable, which then shows additional details on the room.
        private void MoreDetailsPressed(HotelRoom room)
        {
            room.ShowDetails = !room.ShowDetails;
        }

        protected string CheckVacancy(HotelRoom room)
        {
            if (room.OngoingReservation != null)
                return $"Occupied - Ends at {room.OngoingReservation.GetEndDateString()}";  
                
            else if (room.UpcomingReservations.Count > 0)
                return $"Free; Next reservation at {room.UpcomingReservations[0].GetStartDateString()}";

            else
                return "Free";  
        }

        protected string GetBedCount(string roomName)
        {
            HotelRoom? room = DataService.HotelRooms.FirstOrDefault(x => x.RoomNumber == roomName);

            if (room == null)
                return "No Beds Found";

            return room.BedCount.ToString();
        }

        protected string GetClientFullName(string personID)
        {
            if (DataService.Clients.TryGetValue(personID, out Client? value))
                return $"{value.FirstName} {value.LastName}";

            return "No Name Found";
        }

        protected string GetClientEmail(string personID)
        {
            if (DataService.Clients.TryGetValue(personID, out Client? value))
                return value.Email;

            return "No Email Found";
        }

        protected float GetCost(Reservation res)
        {
            HotelRoom? room = DataService.HotelRooms.FirstOrDefault(x => x.RoomNumber == res.RoomName);
            if (room == null)
                return -1;

            return HelperFunctions.GetReservationCost(room.DayCost, res.StartDate, res.EndDate);
        }

        protected void OnCurrentReservationsClicked()
        {
            ShowCurrentReservations = true;
            ShowAllReservations = false;
        }

        protected void OnAllReservationsClicked()
        {
            ShowCurrentReservations = false;
            ShowAllReservations = true;
        }

        protected string GetReservationStatus(Reservation res)
        {
            DateTime today = HelperFunctions.GetEETDateTimeToday();

            if (today < res.StartDate)
                return "Upcoming";
            else if (today > res.EndDate)
                return "Past";
            else
                return "Ongoing";
        }

        private bool FilterFunc1(Reservation res) => FilterFunc(res, searchString1);

        private bool FilterFunc(Reservation res, string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString))
                return true;
            if (res.PersonID.Contains(searchString, StringComparison.OrdinalIgnoreCase))
                return true;
            if (res.RoomName.Contains(searchString, StringComparison.OrdinalIgnoreCase))
                return true;
            if (GetClientFullName(res.PersonID).Contains(searchString, StringComparison.OrdinalIgnoreCase))
                return true;
            if (GetClientEmail(res.PersonID).Contains(searchString, StringComparison.OrdinalIgnoreCase))
                return true;

            return false;
        }
    }
}

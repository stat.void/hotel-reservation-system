﻿using HotelAppServerSide.Data;
using HotelData.DatabaseAccess;
using MudBlazor;
using System.ComponentModel.DataAnnotations;

namespace HotelAppServerSide.Components.Pages
{
    public partial class ViewReservations
    {
        protected string? FirstName { get; set; }
        protected string? LastName { get; set; }
        protected string? PersonID { get; set; }
        protected string? Email { get; set; }

        protected bool[] ShowErrors = [false, false, false, false];

        protected bool ShowSearchResults = false;
        protected bool ShowNotFoundMessage = false;
        protected bool ShowDeletedMessage = false;

        protected bool SearchButtonDisabled = false;
        protected bool DeleteButtonDisabled = false;

        protected readonly List<Reservation> NextReservation = [];
        protected readonly List<Reservation> PastReservations = [];


        private readonly MudTheme Theme = new(); 

        protected async Task OnSearchClicked()
        {
            ShowSearchResults = false;
            ShowNotFoundMessage = false;
            ShowDeletedMessage = false;
            NextReservation.Clear();
            PastReservations.Clear();

            FirstName = FirstName?.Trim();
            LastName = LastName?.Trim();
            PersonID = PersonID?.Trim();
            Email = Email?.Trim();

            ShowErrors[0] = string.IsNullOrWhiteSpace(FirstName);
            ShowErrors[1] = string.IsNullOrWhiteSpace(LastName);
            ShowErrors[2] = string.IsNullOrWhiteSpace(PersonID);
            ShowErrors[3] = string.IsNullOrWhiteSpace(Email);

            if (ShowErrors.Any(x => x))
                return;

            else if (!new EmailAddressAttribute().IsValid(Email))
            {
                ShowErrors[3] = true;
                return;
            }

            SearchButtonDisabled = true;

            // Update local database read status.
            var context = await HotelDataContextFactory.CreateDbContextAsync();
            await DataService.UpdateReadData(context);
            await context.DisposeAsync();

            // Check that the client data matches.
            if (!DataService.Clients.ContainsKey(PersonID))
            {
                ShowNotFoundMessage = true;
                SearchButtonDisabled = false;
                return;
            }

            Client? client = DataService.Clients[PersonID];
            if (client == null ||
                client.FirstName != FirstName ||
                client.LastName != LastName ||
                client.Email != Email?.ToLower())
            {
                ShowNotFoundMessage = true;
                SearchButtonDisabled = false;
                return;
            }

            DateTime today = DateTime.Today;

            // Search the database for any existing values.
            foreach (Reservation res in DataService.ClientReservations)
            {
                if (res.PersonID != PersonID)
                    continue;

                if (res.EndDate < today)
                    PastReservations.Add(res);
                else
                    NextReservation.Add(res);
            }

            if (NextReservation == null && PastReservations.Count == 0)
            {
                ShowNotFoundMessage = true;
                SearchButtonDisabled = false;
                return;
            }

            ShowSearchResults = true;
            SearchButtonDisabled = false;
        }

        protected bool CanDelete(Reservation res)
        {
            DateTime today = HelperFunctions.GetEETDateTimeToday();
            DateTime start = (DateTime) res.StartDate;

            if (start.Subtract(today).Days > 3)
                return true;

            return false;
        }

        protected async void DeleteNextReservation(Reservation res)
        {
            DeleteButtonDisabled = true;
            (bool success, FailType failType) = await DataService.DeleteReservation(HotelDataContextFactory, res);
            if (success)
            {
                ShowDeletedMessage = true;
                ShowSearchResults = false;
            }
            else
            {
                ShowNotFoundMessage = true;
            }

            DeleteButtonDisabled = false;
        }
    }
}

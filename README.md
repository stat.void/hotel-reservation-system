# Hotel Reservation System

A demo application for a "hotel" that allows making and reading reservations made by clients, as well as storing all of this information in a local database that can be read by the web app. The application itself is built with ASP.NET 8, using Blazor as the framework of choice (mainly so that I could get used to its frontend display nuances faster given the time constraints).

## Running the Application
### Visual Studio
The application itself was written by using Visual Studio 2022 for Windows. With the Visual Studio installer, you should make sure to also include the "ASP.NET and web development" package that has all the components specifically for web development. After that, you should be able to simply go to "hotel-reservation-system-main/HotelAppServerSide/HotelAppServerSide.csproj" and open the project up through that file, either by clicking on it directly and choosing to open it with VS2022, or in the program, through "File->Open->Project/Solution".

After that, the only thing that should be needed to do is to press the play icon located on the top side of the window, or pressing F5.

### Visual Studio code
Alternatively, the application can also be run through VS Code for all platforms, but I'm not 100% on all the details to make sure it's guaranteed to work.

I think that the "C# Dev Kit" extension will need to be installed for it to be run, which also installs other mandatory extensions to be able to run C# code. You will also need to download the .NET SDK to be able to run dotnet commands (VS Code should mention the entire process by opening a new window.)

From that point, I got the project running by picking the "Run and Debug" option from the left of the window, clicking the button to run and debug it, and selecting C# as the debugger and using the default configuration of the project, at which point it ran the necessary dotnet commands to open up the web application.

(Extra note to self) - I tried this out on Linux, and it turns out that a database file is generated at the root folder of the application, which has the actual path as the name of the file. I'm currently unsure why this difference exists, but the database functions normally on these platforms when the contents of the database file is overwritten to this location.

## Database 
The local database was built with SQLite and the database file itself is contained inside "hotel-reservation-system-main/HotelAppServerSide/Data/DatabaseAccess/Hotel-Data.db". The application has been set up to search for the file itself at that exact location.

The contents of the database can be divided into 2 sections:
* The ASP.NET section - Consisting of 7 tables, this is just the default database system used to handle login and registration info. All the important information like passwords are properly secured.
* The actual database for the application - consists of 3 tables. "HotelRooms" is used to store rooms based on their name, bed count and daily cost. "Clients" is used to store the client information - Their Person ID, first name, last name and email. "Reservations" is used to store information on what room is being reserved, which person is reserving it through their Person ID, and dates for when it starts and ends.

## Use Cases (Based on the requirements document)
### The user gets an overview of free rooms within a period.
When the web app is started, the user is put to the home page. On the center there is a button that can be clicked to go to the reservation creation page. Alternatively on the navbar, there is a "new" icon that also redirects there.

On this new page, you can pick the date range (starting from tomorrow as the minimum) and when clicking on "Check availability", all the rooms that are available to make a reservation on are shown on the right side. If there are no rooms available within the provided time period, a warning is shown to notify the user (The base data in the database has a case for this around 02.April.2024)

### The user reserves a 1-3 bed count room
Continuing from the above info, when clicking on any of the rooms, the left side of the window opens an additional box, showing which room was selected and giving text fields for first name, last name, Person ID and email. 

The Person ID does checks to verify that the month and day parts of it are a valid Person ID. I decided not to add the checks for the final digit just so that adding client data wouldn't become too annoying. 

The email field also has checks to make sure a valid email is written.

Once confirmed, if there are no conflicts, the reservation is successfully made and the user is notified of it. Other noteworthy things:

* If the Person ID is already used, there is an additional check for all the other data to confirm that it matches. Otherwise it sends a warning that the Person ID exists, but something from the other data doesn't match, blocking reservation creation.

* If all the data matches, the user is still not allowed to make a reservation if they have made a reservation that has not been either cancelled or finished.

* If someone manages to make a reservation before (through a separate web browser window), trying to make the reservation will throw a warning that the time has already been filled and to pick another room. This prevents overlapping reservations from being made.

### The user reserves the cheapest room.
The table that shows all available reservations for the client can be sorted to display the cheapest room. All table columns can be clicked on to sort based on them, but the total reservation price specifically also mentions "Click to sort".

### The user watches their reservation data
On the home screen, there is a button to view reservation data, as well as the nav bar having a magnifying glass icon to go to this view. Here, the user needs to insert the data they used which is then matched against the database. If it matches, a box will appear showing the general data of the reservation.

There is a theoretical data risk that someone else might know this data and find reservations for those users, but I checked out some other real production hotel websites and they generally have a similar approach. If a user had to make an account, it might be too much of a headache for just a one-time reservation.

Also note here, this window shows them past reservations too. An example user for this is:
* First name - Third
* Last name - Ear
* Person ID - 35501028572
* Email - third@ear.com

### The user cancels a reservation.
If there are more than 3 days left before the reservation, then in the view window, a cancel button is visible that deletes it. 

Note that the visibility for the cancel button specifically calls for the date of today in Eastern European timezones, so that button visibility can't be bypassed by temporarily changing the time zone. 

Another note here is that reservations in general don't have clock times on them. I did this on purpose with the intent of "realism", looking at other hotels. A starting clock is not added so that the clients could check in at any general time point to their room. An end date clock is also not added with the intent that the room will need to be cleaned up afterwards.

### Workers have a view for viewing a list of reservations for all clients.
In order for the necessary window to be usable, you need to be logged in (There is also a specific authorization block added so that if you know the webpage link, you will be auto-redirected to the login screen regardless).

On the top right, there is a person icon. It takes you to the default login screen that came with the app. You can register a new account and verify it, which will all work properly. If you don't want to do that, there is a test account with the email "hotel@employee.com" and password "Qwerty123."

Once that's done, the general search icon is replaced with a person search icon. The new window has 2 modes for viewing data. 

The first choice is supposed to be a general overview where you can see every room and what their current availability status is - Is the room currently reserved, and if there are any upcoming reservations.

The second choice opens up the entire reservation database, showing all past, present and future reservations in a list, where all of the data in the table can be sorted, and a search box can be used to filter out for certain specific data (Just make sure to click Enter after you've written a filter).
